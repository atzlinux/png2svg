module github.com/xyproto/png2svg

go 1.18

require (
	github.com/urfave/cli/v2 v2.25.1
	github.com/xyproto/palgen v1.4.0
	github.com/xyproto/tinysvg v1.1.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/lucasb-eyer/go-colorful v1.2.0 // indirect
	github.com/peterhellberg/gfx v0.0.0-20210905153911-4a6ef1535e02 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	github.com/xyproto/burnpal v0.0.0-20191014132200-bd9858647795 // indirect
)
